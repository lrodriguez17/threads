#include <stdio.h>
#include <math.h>
#include <pthread.h> 
#include <stdlib.h>

#define NUM_THREADS 6 //cuantos
#define ITERACIONES 10000
/*Usando Formula Leibniz*/
float valorpi;
float calcPiSerial (int iteraciones){
    int i;
    valorpi = 0;

    for (i = 0; i <= iteraciones; i++){
        valorpi +=   4*  ((pow(-1, i))/(2*i +1));   
    }
    printf("El valor de pi serial utilizando %i iteraciones es: %f \n",iteraciones, valorpi);
    return valorpi;
}
 
 void *calcPiParalelo (void *ID){
    long actualthread =  (long)ID;
    int iter_thread = ITERACIONES / NUM_THREADS;
    int primera_iter = actualthread * iter_thread;
    int ultima_iter = primera_iter + iter_thread;
     //printf("primera: %i \n",primera_iter); 
     //printf("ult: %i \n",ultima_iter); }
    pthread_mutex_t mutex;

    pthread_mutex_lock(&mutex);
    for (int i = primera_iter; i < ultima_iter; i++){
        valorpi += 4* ((pow(-1, i))/(2*i +1)); 
        //printf("El valor de pi paralelo utilizando %i iteraciones es: %f \n",ITERACIONES, valorpi);      
    }
    pthread_mutex_unlock(&mutex);
} 


int compare(){
    float piserial;
    piserial = calcPiSerial(ITERACIONES);

    /*Inicializamos los threads que queremos*/
     valorpi = 0;
     pthread_t threads [NUM_THREADS];

     for (int i = 0; i < NUM_THREADS; i++) {
         pthread_create(&threads[i], NULL, calcPiParalelo, (void *)i); 
     }
     printf("El valor de pi paralelo utilizando %i iteraciones es: %f \n",ITERACIONES, valorpi);      
    if (piserial == valorpi){
        return 0;
    }
    else {
        return 1;
    }

}

void main (){ 
    int x = compare();     
    printf ("Num: %i", x);
  
}

