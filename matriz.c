#include <stdio.h>
#include <math.h>
#include <pthread.h> 
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#define NUM_THREADS 8 //cuantos

void *mult(void* arg) 
{ 
    int *data = (int *)arg; 
    int k = 0, i = 0; 
      
    int x = data[0]; 
    for (i = 1; i <= x; i++) 
           k += data[i]*data[i+x]; 
      
    int *p = (int*)malloc(sizeof(int)); 
         *p = k; 
      
//Used to terminate a thread and the return value is passed as a pointer 
    pthread_exit(p); 
} 

void printM(int* matriz, int n){
    for(int i = 0; i < n; i++){
        printf("%d", matriz[i]);
        printf(", ");
    }
    printf("\n");
}

void matrizSerie(int* vector, int n , int matrix[][n]){
    bool debug = false;
    if(debug == true){
        printf("Vector: ");
        printM(vector, n);


        printf("Matriz: ");
        for(int i = 0; i < n; i++){
            printf("Fila: ");
            printf("%d", i);
            printf("\n");
            for (int j = 0; j < n; j++)
            {
                printf("%d", matrix[i][j]);
                printf("\n");
            }
            
        }
    }
   
    int result[n];
    for(int a = 0; a < n; a++){
        int suma = 0;
        for (int b = 0; b < n; b++)
        {
            suma += vector[b] * matrix[b][a];
        }
        result[a] = suma;
        
    }
    // Imprimir
    printf("El resultado de la matriz es: ");
    printM(result, n);

}


int main (){ 

    int n = rand()%6+3;
    int vector[n];
    int matrix[n][n];
    srand(time(NULL));
    int i, j;
    for (i = 0; i < n; i++)
    {
        vector[i] = rand()%100;
        for (j = 0; j < n; j++)
        {
            matrix[i][j] = rand()%100;
        }
    }
    matrizSerie(vector, n, matrix);
// Para Paralelo
     //declaring array of threads of size n       
    pthread_t *threads; 
    threads = (pthread_t*)malloc(n*sizeof(pthread_t)); 
      
    int count = 0;
    int k; 
    int* data = NULL;  
    for (j = 0; j < n; j++){ 
                
        //storing row and column elements in data  
        data = (int *)malloc((20)*sizeof(int)); 
        data[0] = n; 
        
        for (k = 0; k < n; k++) 
            data[k+1] = vector[k]; 
    
        for (k = 0; k < n; k++) 
            data[k+n+1] = matrix[k][j]; 
            
            //creating threads 
            pthread_create(&threads[count++], NULL,  
                            mult, (void*)(data)); 
                
    }

    printf("Resultado de la matriz con Paralelismo \n"); 
    for (i = 0; i < n; i++){ 
    void *k; 
    
    //Joining all threads and collecting return value  
    pthread_join(threads[i], &k); 
             
            
    int *p = (int *)k; 
    printf("%d ",*p);  
    }  
    printf("\n");





    return 0;
  
}

